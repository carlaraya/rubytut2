class Post < ActiveRecord::Base
  validates :user_id, presence: true
  validates :image, presence: true
  validates :caption, presence: true, length: { maximum: 300 }

  has_attached_file :image, styles: { :medium => "640x" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  acts_as_votable

  belongs_to :user
  has_many :comments, dependent: :destroy

  @@comment_limit = 4

  def last_comments
    comments.order("created_at ASC").last(@@comment_limit)
  end

  def self.comment_limit
    @@comment_limit
  end
end
