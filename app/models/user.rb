class User < ActiveRecord::Base
  validate :user_name_cannot_be_this

  acts_as_voter

  has_attached_file :image, styles: { medium: "640x", thumb: "160x160#" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates :user_name, presence: true, length: { minimum: 4, maximum: 16 }
  validates :bio, length: { maximum: 300 }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy


  def user_name_cannot_be_this
    invalid_user_names = ['users', 'posts']
    if invalid_user_names.include? user_name
      errors.add(:user_name, "Invalid username")
    end
  end
end

