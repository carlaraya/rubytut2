class ProfilesController < ApplicationController
  before_action :set_user
  before_action :require_owner, only: [:edit, :update]
  before_action :set_curr_image, only: [:show, :edit]

  def show
    @posts = @user.posts.order('created_at DESC').page params[:page]
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:success] = "Your profile has been updated!"
      redirect_to(profile_path(@user.user_name))
    else
      flash.now[:alert] = "Update failed. Please check your input."
      render :edit
    end
  end

  private
  def set_user
    @user = User.find_by(user_name: params[:user_name])
  end

  def require_owner
    unless @user == current_user
      flash[:alert] = "Can't hax this"
      redirect_to root_path
    end
  end

  def user_params
    params.require(:user).permit(:image, :bio)
  end

  def set_curr_image
    if @user.image.exists?
      @curr_image = @user.image.url(:thumb)
    else
      @curr_image = 'defaultprofilepic.jpg'
    end
  end
end
