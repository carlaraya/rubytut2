class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: [:show, :like, :edit, :update, :destroy]
  before_action :require_owner, only: [:edit, :update, :destroy]


  def index
    @posts = Post.all.order('created_at DESC').page params[:page]
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)

    if @post.save
      flash[:success] = "Your post has been created with caption: " + @post.caption
      redirect_to posts_path
    else
      flash.now[:alert] = "Your new post could not be created. Please check your input."
      render :new
    end
  end

  def show
  end

  def like
    if current_user.voted_for? @post
      like_success = @post.unliked_by current_user
    else
      like_success = @post.liked_by current_user
    end
    if like_success
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:success] = "Your post has been updated!"
      redirect_to(post_path(@post))
    else
      flash.now[:alert] = "Update failed. Please check your input."
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private
  def require_owner
    unless @post.user == current_user
      flash[:alert] = "Can't hax this"
      redirect_to root_path
    end
  end

  def post_params
    params.require(:post).permit(:image, :caption)
  end

  def set_post
    @post = Post.find(params[:id])
  end
end
